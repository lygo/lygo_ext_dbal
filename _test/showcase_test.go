package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_commons"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_showcase_search"
	"fmt"
	"testing"
	"time"
)

func TestShowcase(t *testing.T) {
	cfg, err := loadConfiguration()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("TESTING SHOWCASE:", cfg)

	engine, err := dbal_showcase_search.NewShowcaseEngine(cfg)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("ENGINE: ", engine)

	// put some data
	count := 0
	for _, category := range dbal_commons.CATEGORIES {
		for i := 0; i < 10; i++ {
			collection := fmt.Sprintf("coll_%v", category)
			key := fmt.Sprintf("entity_%v_%v", category, i)
			payload := map[string]interface{}{
				"collection": collection,
				"key":        key,
				"mode":       "test",
				"category":   category,
			}
			_, _ = engine.Put(payload, time.Now().Unix(), category)

			count++
		}
	}
	fmt.Println("ADDED ITEMS:", count)

	// wait a moment
	time.Sleep(3 * time.Second)

	engine.SetCategoryWeight(dbal_commons.CAT_EVENT, false, 10)
	weight := engine.Categories().Get(dbal_commons.CAT_EVENT).WeightOutDate
	if weight != 10 {
		t.Error(fmt.Sprintf("Expected weight %v, got %v", 10, weight))
		t.FailNow()
	}
	engine.Reset()

	// get data for board
	session := "user1234"
	// change event weight for single user
	_ = engine.SetSessionCategoryWeight(session, dbal_commons.CAT_EVENT, false, 10)
	engine.SetAutoResetSession(true)

	fmt.Println("START ----------")
	items := engine.Query(session, 50)
	for _, item := range items {
		fmt.Println(lygo_json.Stringify(item))
	}
	fmt.Println("END ----------", len(items))

	fmt.Println("START ----------")
	items = engine.Query(session, 50)
	for _, item := range items {
		fmt.Println(lygo_json.Stringify(item))
	}
	fmt.Println("END ----------", len(items))

	fmt.Println("START ----------")
	items = engine.Query(session, 50)
	if len(items) == 0 {
		t.Error("Expected some items")
		t.FailNow()
	}
	for _, item := range items {
		fmt.Println(lygo_json.Stringify(item))
	}
	fmt.Println("END ----------", len(items))

	// remove items one by one
	for _, item := range items {
		key := lygo_reflect.GetString(item, "_key")
		entity, _ := engine.Delete(key)
		payload := lygo_reflect.Get(entity, "payload")
		fmt.Println(lygo_json.Stringify(payload))
	}

	// reset the session
	engine.ResetSession(session)
	engine.SetAutoResetSession(false)
	fmt.Println("START ----------")
	items = engine.Query(session, 50)
	for _, item := range items {
		fmt.Println(lygo_json.Stringify(item))
	}
	fmt.Println("END ----------", len(items))
	fmt.Println("START ----------")
	items = engine.Query(session, 50)
	for _, item := range items {
		fmt.Println(lygo_json.Stringify(item))
	}
	fmt.Println("END ----------", len(items))

}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func loadConfiguration() (*dbal_commons.SemanticConfigDb, error) {
	text, err := lygo_io.ReadTextFromFile("./dsn.json")
	if nil != err {
		return nil, err
	}
	var response *dbal_commons.SemanticConfigDb
	lygo_json.Read(text, &response)
	return response, err
}
