module bitbucket.org/lygo/lygo_ext_dbal

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	bitbucket.org/lygo/lygo_ext_dbbolt v0.1.3
	github.com/arangodb/go-driver v0.0.0-20210825071748-9f1169c6a7dc
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.3.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	go.etcd.io/bbolt v1.3.6 // indirect
)
