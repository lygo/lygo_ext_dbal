package dbal_drivers

import (
	"bitbucket.org/lygo/lygo_commons/lygo"
	"bitbucket.org/lygo/lygo_commons/lygo_crypto"
	"bitbucket.org/lygo/lygo_commons/lygo_regex"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_commons"
	"strings"
)

type ForEachCallback func(map[string]interface{}) bool // if return TRUE, exit loop

type IDatabase interface {
	Uid() string
	DriverName() string
	Enabled() bool
	Open() error
	Close() error

	Upsert(collection string, doc map[string]interface{}) (map[string]interface{}, error)
	Remove(collection string, key string) error
	Get(collection string, key string) (map[string]interface{}, error)
	ForEach(collection string, callback ForEachCallback) error
	Find(collection string, fieldName string, fieldValue interface{}) (interface{}, error)

	// optional methods. May be not supported from all databases
	EnsureIndex(collection string, typeName string, fields []string, unique bool) (bool, error)
	EnsureCollection(collection string) (bool, error)

	// native methods does not support cross-database
	ExecNative(command string, bindingVars map[string]interface{}) (interface{}, error)
	ExecMultiple(commands []string, bindVars []map[string]interface{}, options interface{}) ([]interface{}, error)

	// utils
	QueryGetParamNames(query string) []string
	QuerySelectParams(query string, allParams map[string]interface{}) map[string]interface{}
}

//----------------------------------------------------------------------------------------------------------------------
//	I N I T
//----------------------------------------------------------------------------------------------------------------------

var cache *CacheManager

func init() {
	cache = NewCache()
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

// return a thread safe cache manager
func Cache() *CacheManager {
	return cache
}

func NewDatabase(driverName, connectionString string) (driver IDatabase, err error) {
	dsn := dbal_commons.NewDsn(connectionString)
	return NewDatabaseFromDsn(driverName, dsn)
}

func NewDatabaseFromDsn(driverName string, dsn *dbal_commons.Dsn) (driver IDatabase, err error) {
	if dsn.IsValid() {
		switch driverName {
		case NameArango:
			driver = NewDriverArango(dsn)
			err = driver.Open()
		case NameBolt:
			driver = NewDriverBolt(dsn)
			err = driver.Open()
		case NameMySQL, NameOracle:
			// SQL database
			driver = NewDriverSQL(driverName, dsn)
			err = driver.Open()
		case NameMsSQL, NameODBC:
			// ODBC, MsSQL
			driver = NewDriverODBC(driverName, dsn)
			err = driver.Open()
		default:
			driver = nil
			err = dbal_commons.ErrorDriverNotImplemented
		}

		return driver, err
	}
	return driver, err
}

func OpenDatabase(driver, connectionString string) (IDatabase, error) {
	db, err := NewDatabase(driver, connectionString)
	if nil != err {
		return nil, err
	}
	err = db.Open()
	if nil != err {
		return nil, err
	}
	return db, nil
}

//----------------------------------------------------------------------------------------------------------------------
//	u t i l s
//----------------------------------------------------------------------------------------------------------------------

// QueryGetParamNames return unique param names
func QueryGetParamNames(query string) []string {
	response := make([]string, 0)
	query = strings.ReplaceAll(query, ";", " ;")
	query += " "
	params := lygo_regex.TextBetweenStrings(query, "@", " ")
	for _, param := range params {
		if lygo.Arrays.IndexOf(param, response)==-1{
			response = append(response, param)
		}
	}
	return response
}

func QuerySelectParams(query string, allParams map[string]interface{}) map[string]interface{} {
	names := QueryGetParamNames(query)
	params := map[string]interface{}{}
	for _, v := range names {
		params[v] = allParams[v]
	}
	return params
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func keyFrom(driver, dsn string) string {
	return lygo_crypto.MD5(driver + dsn)
}
