package dbal_drivers

import (
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_rnd"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_commons"
	"bitbucket.org/lygo/lygo_ext_dbbolt"
	"encoding/json"
)

const NameBolt = "bolt"

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type DriverBolt struct {
	uid string
	dsn *dbal_commons.Dsn
	db  *lygo_ext_dbbolt.BoltDatabase
	err error
}

func NewDriverBolt(dsn ...interface{}) *DriverBolt {
	instance := new(DriverBolt)
	if len(dsn) == 1 {
		if s, b := dsn[0].(string); b {
			instance.dsn = dbal_commons.NewDsn(s)
		} else if d, b := dsn[0].(dbal_commons.Dsn); b {
			instance.dsn = &d
		} else if d, b := dsn[0].(*dbal_commons.Dsn); b {
			instance.dsn = d
		} else {
			instance.err = dbal_commons.ErrorInvalidDsn
		}
	}
	if nil == instance.dsn && nil == instance.err {
		instance.err = dbal_commons.ErrorInvalidDsn
	}
	if nil != instance.dsn {
		instance.uid = keyFrom(NameBolt, instance.dsn.String())
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *DriverBolt) Uid() string {
	return instance.uid
}

func (instance *DriverBolt) DriverName() string {
	return NameBolt
}

func (instance *DriverBolt) Enabled() bool {
	return nil != instance && nil != instance.dsn && nil == instance.err && instance.dsn.IsValid()
}

func (instance *DriverBolt) Open() error {
	if nil != instance {
		if nil == instance.err {
			filename := lygo_paths.Absolute(instance.dsn.Database)
			err := lygo_paths.Mkdir(filename)
			if nil != err {
				instance.err = err
			} else {
				config := lygo_ext_dbbolt.NewBoltConfig()
				config.Name = filename
				instance.db = lygo_ext_dbbolt.NewBoltDatabase(config)
				instance.err = instance.db.Open()
			}
		}
		return instance.err
	}
	return nil
}

func (instance *DriverBolt) Close() error {
	if nil != instance && nil != instance.db {
		return instance.db.Close()
	}
	return nil
}

func (instance *DriverBolt) Remove(collection string, key string) error {
	if nil != instance && nil != instance.db {
		return instance.remove(collection, key)
	}
	return dbal_commons.ErrorDatabaseDoesNotExists
}

func (instance *DriverBolt) Get(collection string, key string) (map[string]interface{}, error) {
	if nil != instance && nil != instance.db {
		coll, err := instance.db.Collection(collection, true)
		if nil != err {
			return nil, err
		}
		item, err := coll.Get(key)
		if nil != err {
			return nil, err
		}
		if v, b := item.(map[string]interface{}); b {
			return v, nil
		}
		return nil, nil
	}
	return nil, dbal_commons.ErrorDatabaseDoesNotExists
}

func (instance *DriverBolt) Upsert(collection string, doc map[string]interface{}) (map[string]interface{}, error) {
	if nil != instance && nil != instance.db {
		coll, err := instance.db.Collection(collection, true)
		if nil != err {
			return nil, err
		}

		if _, b := doc["_key"]; !b {
			doc["_key"] = lygo_rnd.Uuid()
		}

		err = coll.Upsert(doc)
		if nil != err {
			return nil, err
		}
		return doc, nil
	}
	return nil, nil
}

func (instance *DriverBolt) ForEach(collection string, callback ForEachCallback) error {
	if nil != instance && nil != instance.db {
		if nil != callback {
			coll, err := instance.db.Collection(collection, true)
			if nil != err {
				return err
			}
			var doc map[string]interface{}
			err = coll.ForEach(func(k, v []byte) bool {
				e := json.Unmarshal(v, &doc)
				if nil != e {
					err = e
					return true // exit
				}
				return callback(doc)
			})

			return err
		}
		return nil
	}
	return dbal_commons.ErrorDatabaseDoesNotExists
}

func (instance *DriverBolt) ExecNative(command string, bindingVars map[string]interface{}) (interface{}, error) {
	if nil != instance && nil != instance.db {

		return nil, dbal_commons.ErrorCommandNotSupported
	}
	return nil, dbal_commons.ErrorDatabaseDoesNotExists
}

func (instance *DriverBolt) ExecMultiple(commands []string, bindVars []map[string]interface{}, options interface{}) ([]interface{}, error) {
	if nil != instance && nil != instance.db {

		return nil, dbal_commons.ErrorCommandNotSupported
	}
	return nil, dbal_commons.ErrorDatabaseDoesNotExists
}

func (instance *DriverBolt) EnsureIndex(collection string, typeName string, fields []string, unique bool) (bool, error) {
	if nil != instance && nil != instance.db {
		_, err := instance.db.Collection(collection, true)
		if nil != err {
			return false, err
		}
		return true, nil
	}
	return false, dbal_commons.ErrorDatabaseDoesNotExists
}

func (instance *DriverBolt) EnsureCollection(collection string) (bool, error) {
	if nil != instance && nil != instance.db {
		_, err := instance.db.Collection(collection, true)
		if nil != err {
			return false, err
		}
		return true, nil
	}
	return false, dbal_commons.ErrorDatabaseDoesNotExists
}

func (instance *DriverBolt) Find(collection string, fieldName string, fieldValue interface{}) (interface{}, error) {
	if nil != instance && nil != instance.db {
		coll, err := instance.db.Collection(collection, true)
		if nil != err {
			return nil, err
		}
		return coll.GetByFieldValue(fieldName, fieldValue)
	}
	return nil, dbal_commons.ErrorDatabaseDoesNotExists
}

func (instance *DriverBolt) QueryGetParamNames(query string) []string {
	return QueryGetParamNames(query)
}

func (instance *DriverBolt) QuerySelectParams(query string, allParams map[string]interface{}) map[string]interface{} {
	return QuerySelectParams(query, allParams)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *DriverBolt) remove(collectionName string, key string) (err error) {
	if nil != instance && nil != instance.db {
		var coll *lygo_ext_dbbolt.BoltCollection
		coll, err = instance.db.Collection(collectionName, true)
		if nil == err {
			err = coll.Remove(key)
		}
	}
	return err
}
