package dbal_semantic_search_test

import (
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_semantic_search"
	"fmt"
	"testing"
)

func TestToKeywords(t *testing.T) {
	keywords := dbal_semantic_search.ToKeywords("hello this is a text to tokenize in keywords!!")
	fmt.Println(keywords)
}