# Database Abstraction Layer #

![](./icon.png)

This module implements a Database Abstraction Layer with some more utilities:
- Elastic Search: full text search with result scoring
- Showcase: bulletin board implementation. Content are saved once. At each query some contents are returned
- More to come....

## Dependencies ##

```
go get -u bitbucket.org/lygo/lygo_commons
```

## Drivers ##

```
go get -u github.com/arangodb/go-driver
go get -u github.com/arangodb/go-driver/http

go get -u bitbucket.org/lygo/lygo_ext_dbbolt
```

## How to Use ##

To use just call:

```
go get -u bitbucket.org/lygo/lygo_ext_dbal@v0.1.31
```

### Versioning ##

Sources are versioned using git tags:

```
git tag v0.1.31
git push origin v0.1.31
```
