package lygo_ext_dbal

import (
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_regex"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_commons"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_drivers"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_semantic_search"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_showcase_search"
)

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func NewDatabase(driverName, connectionString string) (driver dbal_drivers.IDatabase, err error) {
	return dbal_drivers.NewDatabase(driverName, connectionString)
}

func NewDatabaseFromDsn(driverName string, dsn *dbal_commons.Dsn) (dbal_drivers.IDatabase, error) {
	return dbal_drivers.NewDatabaseFromDsn(driverName, dsn)
}

func OpenDatabase(driver, connectionString string) (dbal_drivers.IDatabase, error) {
	return dbal_drivers.OpenDatabase(driver, connectionString)
}

func NewSemanticEngine(c interface{}) (*dbal_semantic_search.SemanticEngine, error) {
	config, err := getConfig(c)
	if nil != err {
		return nil, err
	}
	return dbal_semantic_search.NewSemanticEngine(config)
}

func NewShowcaseEngine(c interface{}) (*dbal_showcase_search.ShowcaseEngine, error) {
	config, err := getConfigDB(c)
	if nil != err {
		return nil, err
	}
	return dbal_showcase_search.NewShowcaseEngine(config)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func getConfig(c interface{}) (*dbal_commons.SemanticConfig, error) {
	var config *dbal_commons.SemanticConfig
	if v, b := c.(*dbal_commons.SemanticConfig); b {
		config = v
	} else if s, b := c.(string); b {
		if lygo_regex.IsValidJsonObject(s) {
			err := lygo_json.Read(s, &config)
			if nil != err {
				return nil, err
			}
		} else {
			// file
			err := lygo_json.ReadFromFile(s, &config)
			if nil != err {
				return nil, err
			}
		}
	} else if m, b := c.(map[string]interface{}); b {
		err := lygo_json.Read(lygo_json.Stringify(m), &config)
		if nil != err {
			return nil, err
		}
	} else if mp, b := c.(*map[string]interface{}); b {
		err := lygo_json.Read(lygo_json.Stringify(mp), &config)
		if nil != err {
			return nil, err
		}
	}
	if nil == config {
		return nil, dbal_commons.ErrorMismatchConfiguration
	}
	return config, nil
}

func getConfigDB(c interface{}) (*dbal_commons.SemanticConfigDb, error) {
	var config *dbal_commons.SemanticConfigDb
	if v, b := c.(*dbal_commons.SemanticConfigDb); b {
		config = v
	} else if s, b := c.(string); b {
		if lygo_regex.IsValidJsonObject(s) {
			err := lygo_json.Read(s, &config)
			if nil != err {
				return nil, err
			}
		} else {
			// file
			err := lygo_json.ReadFromFile(s, &config)
			if nil != err {
				return nil, err
			}
		}
	} else if m, b := c.(map[string]interface{}); b {
		err := lygo_json.Read(lygo_json.Stringify(m), &config)
		if nil != err {
			return nil, err
		}
	} else if mp, b := c.(*map[string]interface{}); b {
		err := lygo_json.Read(lygo_json.Stringify(mp), &config)
		if nil != err {
			return nil, err
		}
	}
	if nil == config {
		return nil, dbal_commons.ErrorMismatchConfiguration
	}
	return config, nil
}

