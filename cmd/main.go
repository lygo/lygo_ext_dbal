package main

import (
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_drivers"
	"flag"
	"fmt"
	"os"
)

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := lygo_strings.Format("[panic] APPLICATION %s ERROR: %s", "DBAL", r)
			fmt.Println(message)
		}
	}()

	//-- command flags --//
	// build
	cmdQuery := flag.NewFlagSet("query", flag.ExitOnError)
	cmdQueryDriver := cmdQuery.String("driver", "odbc", "Set Driver to use")
	cmdQueryDsn := cmdQuery.String("dsn", "", "Set DSN for connection")
	cmdQueryCommand := cmdQuery.String("cmd", "", "Write SQL command")
	// publish
	cmdPublish := flag.NewFlagSet("publish", flag.ExitOnError)
	cmdPublishSettings := cmdPublish.String("settings", lygo_paths.Absolute("./_workspace/settings.json"), "Set file to load with required settings")
	cmdPublishDir := cmdPublish.String("dir", lygo_paths.Absolute("./uploads"), "Set directory with post to publish")
	// stub
	cmdStub := flag.NewFlagSet("stub", flag.ExitOnError)
	cmdStubSettings := cmdStub.String("settings", lygo_paths.Absolute("./_workspace/settings.json"), "Set file to load with required settings")
	cmdStubDir := cmdStub.String("dir", "./uploads", "Set target directory")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "query":
			// QUERY
			_ = cmdQuery.Parse(os.Args[2:])

			// run command
			query(*cmdQueryDriver, *cmdQueryDsn, *cmdQueryCommand)
		case "publish":
			// BUILD
			_ = cmdPublish.Parse(os.Args[2:])
			dirWork := lygo_paths.Dir(*cmdPublishSettings)
			// set paths
			lygo_paths.GetWorkspace("*").SetPath(dirWork)
			// run command
			publish(*cmdPublishSettings, *cmdPublishDir)
		case "stub":
			// STUB
			_ = cmdStub.Parse(os.Args[2:])
			dirWork := lygo_paths.Dir(*cmdStubSettings)
			// set paths
			lygo_paths.GetWorkspace("*").SetPath(dirWork)
			dir := *cmdStubDir
			if len(dir) == 0 {
				dir = "./uploads"
			}
			// run command
			stub(*cmdStubSettings, dir)
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func query(driver, dsn, command string) {
	db, err := dbal_drivers.NewDatabase(driver, dsn)
	if nil != err {
		panic(err)
	}
	data, err := db.ExecNative(command, nil)
	if nil != err {
		panic(err)
	}
	fmt.Println(lygo_json.Stringify(data))
}

func publish(file, dir string) {

}

func stub(file, target string) {

}
